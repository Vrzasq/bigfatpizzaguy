﻿namespace BigFatPizzaGuy
{
    partial class HowToPlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HowToPlay));
            this.lbl_howToPlay = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_howToPlay
            // 
            this.lbl_howToPlay.Font = new System.Drawing.Font("Franklin Gothic Heavy", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_howToPlay.Location = new System.Drawing.Point(12, 9);
            this.lbl_howToPlay.Name = "lbl_howToPlay";
            this.lbl_howToPlay.Size = new System.Drawing.Size(690, 486);
            this.lbl_howToPlay.TabIndex = 0;
            this.lbl_howToPlay.Text = resources.GetString("lbl_howToPlay.Text");
            // 
            // HowToPlay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 505);
            this.Controls.Add(this.lbl_howToPlay);
            this.Name = "HowToPlay";
            this.Text = "Big Fat Pizza Guy - How To Play";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_howToPlay;
    }
}