﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigFatPizzaGuy.powerups
{
    class BiggerStomach : PowerUP
    {
        public override string Name { get; set; }
        public override Rectangle Rect
        {
            get { return _Rect; }
        }

        public override void Init(Random rand)
        {
            Name = "stomach";
            Y = rand.Next(1, 430);
            _Rect = new Rectangle(X, Y, Size, Size);
        }

        public override void Update()
        {
            _Rect.X -= Speed;
        }
    }
}
