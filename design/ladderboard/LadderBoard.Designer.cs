﻿namespace BigFatPizzaGuy
{
    partial class LadderBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_RankHeader = new System.Windows.Forms.Label();
            this.lbl_Rank1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_NameHeader = new System.Windows.Forms.Label();
            this.lbl_ScoreHeader = new System.Windows.Forms.Label();
            this.lbl_DateHeader = new System.Windows.Forms.Label();
            this.lbl_Name1 = new System.Windows.Forms.Label();
            this.lbl_Name2 = new System.Windows.Forms.Label();
            this.lbl_Name3 = new System.Windows.Forms.Label();
            this.lbl_Name4 = new System.Windows.Forms.Label();
            this.lbl_Name5 = new System.Windows.Forms.Label();
            this.lbl_Name6 = new System.Windows.Forms.Label();
            this.lbl_Name7 = new System.Windows.Forms.Label();
            this.lbl_Name8 = new System.Windows.Forms.Label();
            this.lbl_Name9 = new System.Windows.Forms.Label();
            this.lbl_Name10 = new System.Windows.Forms.Label();
            this.lbl_Score1 = new System.Windows.Forms.Label();
            this.lbl_Score2 = new System.Windows.Forms.Label();
            this.lbl_Score4 = new System.Windows.Forms.Label();
            this.lbl_Score3 = new System.Windows.Forms.Label();
            this.lbl_Score5 = new System.Windows.Forms.Label();
            this.lbl_Score6 = new System.Windows.Forms.Label();
            this.lbl_Score7 = new System.Windows.Forms.Label();
            this.lbl_Score8 = new System.Windows.Forms.Label();
            this.lbl_Score9 = new System.Windows.Forms.Label();
            this.lbl_Score10 = new System.Windows.Forms.Label();
            this.lbl_Date1 = new System.Windows.Forms.Label();
            this.lbl_Date2 = new System.Windows.Forms.Label();
            this.lbl_Date3 = new System.Windows.Forms.Label();
            this.lbl_Date4 = new System.Windows.Forms.Label();
            this.lbl_Date5 = new System.Windows.Forms.Label();
            this.lbl_Date6 = new System.Windows.Forms.Label();
            this.lbl_Date7 = new System.Windows.Forms.Label();
            this.lbl_Date8 = new System.Windows.Forms.Label();
            this.lbl_Date9 = new System.Windows.Forms.Label();
            this.lbl_Date10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_RankHeader
            // 
            this.lbl_RankHeader.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_RankHeader.Location = new System.Drawing.Point(12, 9);
            this.lbl_RankHeader.Name = "lbl_RankHeader";
            this.lbl_RankHeader.Size = new System.Drawing.Size(76, 31);
            this.lbl_RankHeader.TabIndex = 0;
            this.lbl_RankHeader.Text = "Rank";
            // 
            // lbl_Rank1
            // 
            this.lbl_Rank1.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Rank1.Location = new System.Drawing.Point(12, 40);
            this.lbl_Rank1.Name = "lbl_Rank1";
            this.lbl_Rank1.Size = new System.Drawing.Size(76, 31);
            this.lbl_Rank1.TabIndex = 0;
            this.lbl_Rank1.Text = "1.";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label2.Location = new System.Drawing.Point(12, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 31);
            this.label2.TabIndex = 0;
            this.label2.Text = "2.";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label3.Location = new System.Drawing.Point(12, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 31);
            this.label3.TabIndex = 0;
            this.label3.Text = "4.";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label4.Location = new System.Drawing.Point(12, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "5.";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label5.Location = new System.Drawing.Point(12, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 31);
            this.label5.TabIndex = 0;
            this.label5.Text = "6.";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label6.Location = new System.Drawing.Point(12, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 31);
            this.label6.TabIndex = 0;
            this.label6.Text = "7.";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label7.Location = new System.Drawing.Point(12, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 31);
            this.label7.TabIndex = 0;
            this.label7.Text = "3.";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label8.Location = new System.Drawing.Point(12, 257);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 31);
            this.label8.TabIndex = 0;
            this.label8.Text = "8.";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label9.Location = new System.Drawing.Point(12, 288);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 31);
            this.label9.TabIndex = 0;
            this.label9.Text = "9.";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.label10.Location = new System.Drawing.Point(12, 319);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 31);
            this.label10.TabIndex = 0;
            this.label10.Text = "10.";
            // 
            // lbl_NameHeader
            // 
            this.lbl_NameHeader.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_NameHeader.Location = new System.Drawing.Point(94, 9);
            this.lbl_NameHeader.Name = "lbl_NameHeader";
            this.lbl_NameHeader.Size = new System.Drawing.Size(245, 31);
            this.lbl_NameHeader.TabIndex = 0;
            this.lbl_NameHeader.Text = "Name";
            this.lbl_NameHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_ScoreHeader
            // 
            this.lbl_ScoreHeader.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_ScoreHeader.Location = new System.Drawing.Point(345, 9);
            this.lbl_ScoreHeader.Name = "lbl_ScoreHeader";
            this.lbl_ScoreHeader.Size = new System.Drawing.Size(219, 31);
            this.lbl_ScoreHeader.TabIndex = 0;
            this.lbl_ScoreHeader.Text = "Score";
            this.lbl_ScoreHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_DateHeader
            // 
            this.lbl_DateHeader.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_DateHeader.Location = new System.Drawing.Point(570, 9);
            this.lbl_DateHeader.Name = "lbl_DateHeader";
            this.lbl_DateHeader.Size = new System.Drawing.Size(177, 31);
            this.lbl_DateHeader.TabIndex = 0;
            this.lbl_DateHeader.Text = "Date";
            this.lbl_DateHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name1
            // 
            this.lbl_Name1.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name1.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name1.Location = new System.Drawing.Point(94, 40);
            this.lbl_Name1.Name = "lbl_Name1";
            this.lbl_Name1.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name1.TabIndex = 0;
            this.lbl_Name1.Text = "Name";
            this.lbl_Name1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name2
            // 
            this.lbl_Name2.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name2.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name2.Location = new System.Drawing.Point(94, 71);
            this.lbl_Name2.Name = "lbl_Name2";
            this.lbl_Name2.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name2.TabIndex = 0;
            this.lbl_Name2.Text = "Name";
            this.lbl_Name2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name3
            // 
            this.lbl_Name3.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name3.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name3.Location = new System.Drawing.Point(94, 102);
            this.lbl_Name3.Name = "lbl_Name3";
            this.lbl_Name3.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name3.TabIndex = 0;
            this.lbl_Name3.Text = "Name";
            this.lbl_Name3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name4
            // 
            this.lbl_Name4.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name4.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name4.Location = new System.Drawing.Point(94, 133);
            this.lbl_Name4.Name = "lbl_Name4";
            this.lbl_Name4.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name4.TabIndex = 0;
            this.lbl_Name4.Text = "Name";
            this.lbl_Name4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name5
            // 
            this.lbl_Name5.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name5.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name5.Location = new System.Drawing.Point(94, 164);
            this.lbl_Name5.Name = "lbl_Name5";
            this.lbl_Name5.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name5.TabIndex = 0;
            this.lbl_Name5.Text = "Name";
            this.lbl_Name5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name6
            // 
            this.lbl_Name6.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name6.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name6.Location = new System.Drawing.Point(94, 195);
            this.lbl_Name6.Name = "lbl_Name6";
            this.lbl_Name6.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name6.TabIndex = 0;
            this.lbl_Name6.Text = "Name";
            this.lbl_Name6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name7
            // 
            this.lbl_Name7.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name7.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name7.Location = new System.Drawing.Point(94, 226);
            this.lbl_Name7.Name = "lbl_Name7";
            this.lbl_Name7.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name7.TabIndex = 0;
            this.lbl_Name7.Text = "Name";
            this.lbl_Name7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name8
            // 
            this.lbl_Name8.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name8.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name8.Location = new System.Drawing.Point(94, 257);
            this.lbl_Name8.Name = "lbl_Name8";
            this.lbl_Name8.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name8.TabIndex = 0;
            this.lbl_Name8.Text = "Name";
            this.lbl_Name8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name9
            // 
            this.lbl_Name9.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name9.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name9.Location = new System.Drawing.Point(94, 288);
            this.lbl_Name9.Name = "lbl_Name9";
            this.lbl_Name9.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name9.TabIndex = 0;
            this.lbl_Name9.Text = "Name";
            this.lbl_Name9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Name10
            // 
            this.lbl_Name10.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Name10.ForeColor = System.Drawing.Color.Peru;
            this.lbl_Name10.Location = new System.Drawing.Point(94, 319);
            this.lbl_Name10.Name = "lbl_Name10";
            this.lbl_Name10.Size = new System.Drawing.Size(245, 31);
            this.lbl_Name10.TabIndex = 0;
            this.lbl_Name10.Text = "Name";
            this.lbl_Name10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score1
            // 
            this.lbl_Score1.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score1.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score1.Location = new System.Drawing.Point(345, 40);
            this.lbl_Score1.Name = "lbl_Score1";
            this.lbl_Score1.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score1.TabIndex = 0;
            this.lbl_Score1.Text = "Score";
            this.lbl_Score1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score2
            // 
            this.lbl_Score2.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score2.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score2.Location = new System.Drawing.Point(345, 71);
            this.lbl_Score2.Name = "lbl_Score2";
            this.lbl_Score2.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score2.TabIndex = 0;
            this.lbl_Score2.Text = "Score";
            this.lbl_Score2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score4
            // 
            this.lbl_Score4.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score4.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score4.Location = new System.Drawing.Point(345, 133);
            this.lbl_Score4.Name = "lbl_Score4";
            this.lbl_Score4.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score4.TabIndex = 0;
            this.lbl_Score4.Text = "Score";
            this.lbl_Score4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score3
            // 
            this.lbl_Score3.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score3.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score3.Location = new System.Drawing.Point(345, 102);
            this.lbl_Score3.Name = "lbl_Score3";
            this.lbl_Score3.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score3.TabIndex = 0;
            this.lbl_Score3.Text = "Score";
            this.lbl_Score3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score5
            // 
            this.lbl_Score5.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score5.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score5.Location = new System.Drawing.Point(345, 164);
            this.lbl_Score5.Name = "lbl_Score5";
            this.lbl_Score5.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score5.TabIndex = 0;
            this.lbl_Score5.Text = "Score";
            this.lbl_Score5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score6
            // 
            this.lbl_Score6.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score6.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score6.Location = new System.Drawing.Point(345, 195);
            this.lbl_Score6.Name = "lbl_Score6";
            this.lbl_Score6.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score6.TabIndex = 0;
            this.lbl_Score6.Text = "Score";
            this.lbl_Score6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score7
            // 
            this.lbl_Score7.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score7.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score7.Location = new System.Drawing.Point(345, 226);
            this.lbl_Score7.Name = "lbl_Score7";
            this.lbl_Score7.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score7.TabIndex = 0;
            this.lbl_Score7.Text = "Score";
            this.lbl_Score7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score8
            // 
            this.lbl_Score8.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score8.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score8.Location = new System.Drawing.Point(345, 257);
            this.lbl_Score8.Name = "lbl_Score8";
            this.lbl_Score8.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score8.TabIndex = 0;
            this.lbl_Score8.Text = "Score";
            this.lbl_Score8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score9
            // 
            this.lbl_Score9.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score9.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score9.Location = new System.Drawing.Point(345, 288);
            this.lbl_Score9.Name = "lbl_Score9";
            this.lbl_Score9.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score9.TabIndex = 0;
            this.lbl_Score9.Text = "Score";
            this.lbl_Score9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Score10
            // 
            this.lbl_Score10.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Score10.ForeColor = System.Drawing.Color.SteelBlue;
            this.lbl_Score10.Location = new System.Drawing.Point(345, 319);
            this.lbl_Score10.Name = "lbl_Score10";
            this.lbl_Score10.Size = new System.Drawing.Size(219, 31);
            this.lbl_Score10.TabIndex = 0;
            this.lbl_Score10.Text = "Score";
            this.lbl_Score10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date1
            // 
            this.lbl_Date1.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date1.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date1.Location = new System.Drawing.Point(570, 40);
            this.lbl_Date1.Name = "lbl_Date1";
            this.lbl_Date1.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date1.TabIndex = 0;
            this.lbl_Date1.Text = "Date";
            this.lbl_Date1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date2
            // 
            this.lbl_Date2.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date2.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date2.Location = new System.Drawing.Point(570, 71);
            this.lbl_Date2.Name = "lbl_Date2";
            this.lbl_Date2.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date2.TabIndex = 0;
            this.lbl_Date2.Text = "Date";
            this.lbl_Date2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date3
            // 
            this.lbl_Date3.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date3.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date3.Location = new System.Drawing.Point(570, 102);
            this.lbl_Date3.Name = "lbl_Date3";
            this.lbl_Date3.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date3.TabIndex = 0;
            this.lbl_Date3.Text = "Date";
            this.lbl_Date3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date4
            // 
            this.lbl_Date4.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date4.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date4.Location = new System.Drawing.Point(570, 133);
            this.lbl_Date4.Name = "lbl_Date4";
            this.lbl_Date4.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date4.TabIndex = 0;
            this.lbl_Date4.Text = "Date";
            this.lbl_Date4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date5
            // 
            this.lbl_Date5.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date5.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date5.Location = new System.Drawing.Point(570, 164);
            this.lbl_Date5.Name = "lbl_Date5";
            this.lbl_Date5.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date5.TabIndex = 0;
            this.lbl_Date5.Text = "Date";
            this.lbl_Date5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date6
            // 
            this.lbl_Date6.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date6.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date6.Location = new System.Drawing.Point(570, 195);
            this.lbl_Date6.Name = "lbl_Date6";
            this.lbl_Date6.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date6.TabIndex = 0;
            this.lbl_Date6.Text = "Date";
            this.lbl_Date6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date7
            // 
            this.lbl_Date7.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date7.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date7.Location = new System.Drawing.Point(570, 226);
            this.lbl_Date7.Name = "lbl_Date7";
            this.lbl_Date7.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date7.TabIndex = 0;
            this.lbl_Date7.Text = "Date";
            this.lbl_Date7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date8
            // 
            this.lbl_Date8.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date8.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date8.Location = new System.Drawing.Point(570, 257);
            this.lbl_Date8.Name = "lbl_Date8";
            this.lbl_Date8.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date8.TabIndex = 0;
            this.lbl_Date8.Text = "Date";
            this.lbl_Date8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date9
            // 
            this.lbl_Date9.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date9.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date9.Location = new System.Drawing.Point(570, 288);
            this.lbl_Date9.Name = "lbl_Date9";
            this.lbl_Date9.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date9.TabIndex = 0;
            this.lbl_Date9.Text = "Date";
            this.lbl_Date9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_Date10
            // 
            this.lbl_Date10.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Date10.ForeColor = System.Drawing.Color.OliveDrab;
            this.lbl_Date10.Location = new System.Drawing.Point(570, 319);
            this.lbl_Date10.Name = "lbl_Date10";
            this.lbl_Date10.Size = new System.Drawing.Size(177, 31);
            this.lbl_Date10.TabIndex = 0;
            this.lbl_Date10.Text = "Date";
            this.lbl_Date10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LadderBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 360);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_Rank1);
            this.Controls.Add(this.lbl_Date10);
            this.Controls.Add(this.lbl_Date9);
            this.Controls.Add(this.lbl_Date8);
            this.Controls.Add(this.lbl_Date7);
            this.Controls.Add(this.lbl_Date6);
            this.Controls.Add(this.lbl_Date5);
            this.Controls.Add(this.lbl_Date4);
            this.Controls.Add(this.lbl_Date3);
            this.Controls.Add(this.lbl_Date2);
            this.Controls.Add(this.lbl_Date1);
            this.Controls.Add(this.lbl_DateHeader);
            this.Controls.Add(this.lbl_Score10);
            this.Controls.Add(this.lbl_Score9);
            this.Controls.Add(this.lbl_Score3);
            this.Controls.Add(this.lbl_Score8);
            this.Controls.Add(this.lbl_Score7);
            this.Controls.Add(this.lbl_Score6);
            this.Controls.Add(this.lbl_Score5);
            this.Controls.Add(this.lbl_Score4);
            this.Controls.Add(this.lbl_Score2);
            this.Controls.Add(this.lbl_Score1);
            this.Controls.Add(this.lbl_ScoreHeader);
            this.Controls.Add(this.lbl_Name10);
            this.Controls.Add(this.lbl_Name9);
            this.Controls.Add(this.lbl_Name8);
            this.Controls.Add(this.lbl_Name7);
            this.Controls.Add(this.lbl_Name6);
            this.Controls.Add(this.lbl_Name5);
            this.Controls.Add(this.lbl_Name4);
            this.Controls.Add(this.lbl_Name3);
            this.Controls.Add(this.lbl_Name2);
            this.Controls.Add(this.lbl_Name1);
            this.Controls.Add(this.lbl_NameHeader);
            this.Controls.Add(this.lbl_RankHeader);
            this.Name = "LadderBoard";
            this.Text = "LadderBoard";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_RankHeader;
        private System.Windows.Forms.Label lbl_Rank1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_NameHeader;
        private System.Windows.Forms.Label lbl_ScoreHeader;
        private System.Windows.Forms.Label lbl_DateHeader;
        private System.Windows.Forms.Label lbl_Name1;
        private System.Windows.Forms.Label lbl_Name2;
        private System.Windows.Forms.Label lbl_Name3;
        private System.Windows.Forms.Label lbl_Name4;
        private System.Windows.Forms.Label lbl_Name5;
        private System.Windows.Forms.Label lbl_Name6;
        private System.Windows.Forms.Label lbl_Name7;
        private System.Windows.Forms.Label lbl_Name8;
        private System.Windows.Forms.Label lbl_Name9;
        private System.Windows.Forms.Label lbl_Name10;
        private System.Windows.Forms.Label lbl_Score1;
        private System.Windows.Forms.Label lbl_Score2;
        private System.Windows.Forms.Label lbl_Score4;
        private System.Windows.Forms.Label lbl_Score3;
        private System.Windows.Forms.Label lbl_Score5;
        private System.Windows.Forms.Label lbl_Score6;
        private System.Windows.Forms.Label lbl_Score7;
        private System.Windows.Forms.Label lbl_Score8;
        private System.Windows.Forms.Label lbl_Score9;
        private System.Windows.Forms.Label lbl_Score10;
        private System.Windows.Forms.Label lbl_Date1;
        private System.Windows.Forms.Label lbl_Date2;
        private System.Windows.Forms.Label lbl_Date3;
        private System.Windows.Forms.Label lbl_Date4;
        private System.Windows.Forms.Label lbl_Date5;
        private System.Windows.Forms.Label lbl_Date6;
        private System.Windows.Forms.Label lbl_Date7;
        private System.Windows.Forms.Label lbl_Date8;
        private System.Windows.Forms.Label lbl_Date9;
        private System.Windows.Forms.Label lbl_Date10;
    }
}