﻿using System.Collections;
using System.Windows.Forms;

namespace BigFatPizzaGuy
{
    // enum to define movement
    public enum Directions { Up, Down, Left, Right, UpLeft, UpRight, DownLeft, DownRight, NoDirection };

    internal class Input
    {
        //Load list of available Keyboard buttons
        private static Hashtable keyTable = new Hashtable();

        public static void ClearInputTable()
        {
            keyTable.Clear();
        }

        //Perform a check to see if a particular button is pressed.
        public static bool KeyPressed(Keys key)
        {
            if (keyTable[key] == null)
            {
                return false;
            }
            return (bool)keyTable[key];
        }

        // return Direction of player movement
        public static Directions Direction()
        {
            if (KeyPressed(Keys.W) || KeyPressed(Keys.NumPad8))
                return Directions.Up;
            else if (KeyPressed(Keys.S) || KeyPressed(Keys.NumPad2))
                return Directions.Down;
            else if (KeyPressed(Keys.A) || KeyPressed(Keys.NumPad4))
                return Directions.Left;
            else if (KeyPressed(Keys.D) || KeyPressed(Keys.NumPad6))
                return Directions.Right;
            else if (KeyPressed(Keys.Q) || KeyPressed(Keys.NumPad7))
                return Directions.UpLeft;
            else if (KeyPressed(Keys.E) || KeyPressed(Keys.NumPad9))
                return Directions.UpRight;
            else if (KeyPressed(Keys.Z) || KeyPressed(Keys.NumPad1))
                return Directions.DownLeft;
            else if (KeyPressed(Keys.C) || KeyPressed(Keys.NumPad3))
                return Directions.DownRight;
            else
                return Directions.NoDirection;
        }

        //Detect if a keyboard button is pressed
        public static void ChangeState(Keys key, bool state)
        {
            keyTable[key] = state;
        }
    }
}