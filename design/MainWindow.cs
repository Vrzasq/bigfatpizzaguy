﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BigFatPizzaGuy
{
    public partial class MainWindow : Form, IYolo
    {
        // define game speed / fps
        private const int FPS = 60;
        private Game game;
        public MainWindow()
        {
            InitializeComponent();

            // read ladder baord file
            LadderBoardFileHandler.ReadLadderBoard();

            // Instanciate new game object
            game = new Game(this);
            game.LoadAsstes();
            game.Initialize();

            // attach game loop to a timer tick event
            gameTimer.Tick += game.GameState;
            // set fps of the game
            gameTimer.Interval = game.SetFPS(FPS);
            gameTimer.Start();
        }


        // Close Application if Quit button was clicked
        private void btn_Quit_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Quit button was pressed");
            Application.Exit();
        }

        private void btn_Restart_Click(object sender, EventArgs e)
        {
            if (!btn_Start.Visible)
            {
                game.Initialize();
                game.GameOver = false;
                lbl_GameOver.Visible = false;
                lbl_AngryPizza.Visible = false;
                HidePowersImage();
                if (game.Paused)
                    btn_Pause_Click(this, e);
            }
        }
        private void btn_Start_Click(object sender, EventArgs e)
        {
            lbl_Weclome.Visible = false;
            btn_Start.Visible = false;
            game.GameOver = false;
            btn_Pause.Visible = true;
            btn_Restart.Visible = true;
            pb_GameWindow.Focus();
        }

        private void btn_Pause_Click(object sender, EventArgs e)
        {
            if (!btn_Start.Visible)
            {
                if (!game.Paused)
                {
                    game.Paused = true;
                    btn_Pause.Text = "Continue";
                    btn_Pause.ForeColor = Color.Red;
                }
                else
                {
                    game.Paused = false;
                    btn_Pause.Text = "Pause";
                    btn_Pause.ForeColor = Color.Black;
                }
                pb_GameWindow.Focus();
            }

        }


        // implementation od IYolo interface
        public Image GameWindow
        {
            get { return pb_GameWindow.Image; }
            set { pb_GameWindow.Image = value; }
        }

        public int SetScore
        {
            set { lbl_Score.Text = value.ToString(); }
        }

        public void RefreshImage()
        {
            this.Invalidate();
        }

        public void ShowGameOver(bool state)
        {
            lbl_GameOver.Visible = state;
        }

        public void ShowAngryPizzaMsg(bool state)
        {
            lbl_AngryPizza.Visible = state;
        }

        public void ShowDigestive(bool state)
        {
            lbl_Digestive.Visible = state;
            pb_Digestive.Visible = state;
        }

        public void ShowStomach(bool state)
        {
            lbl_Stomach.Visible = state;
            pb_Stomach.Visible = state;
        }

        public void ShowBoots(bool state)
        {
            lbl_Boots.Visible = state;
            pb_Boots.Visible = state;
        }

        public void ShowPB(bool state)
        {
            lbl_Points.Visible = state;
            pb_Points.Visible = state;
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, true);
            switch (e.KeyCode)
            {
                case Keys.Space:
                    e.SuppressKeyPress = true;
                    btn_Restart_Click(this, e);
                    break;

                case Keys.NumPad5:
                    ShowDigestive(false);
                    game.PlayerUseDigestive();
                    break;
            }
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, false);
        }

        private void MainWindow_Deactivate(object sender, EventArgs e)
        {
            if (!game.Paused)
                btn_Pause_Click(this, e);
        }

        private void btn_HowToPlay_Click(object sender, EventArgs e)
        {
            HowToPlay htp = new HowToPlay();
            htp.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LadderBoard ladder = new LadderBoard();
            ladder.Visible = true;
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            LadderBoardFileHandler.SaveLadderToFile();
        }

        private void HidePowersImage()
        {
            ShowBoots(false);
            ShowDigestive(false);
            ShowPB(false);
            ShowStomach(false);
        }
    }
}