﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigFatPizzaGuy
{
    abstract class PowerUP
    {
        protected int Speed = 5;
        protected int X = 640;
        protected int Y;
        protected int Size = 40;
        protected Rectangle _Rect;
        public abstract void Init(Random rand);
        public abstract void Update();
        public abstract string Name { get; set; }
        public abstract Rectangle Rect { get; }
    }
}
