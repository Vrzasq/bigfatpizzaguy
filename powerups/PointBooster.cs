﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigFatPizzaGuy.powerups
{
    class PointBooster : PowerUP
    {
        private int _Height = 40;
        private int _Widht = 62;
        public override string Name { get; set; }
        public override Rectangle Rect
        {
            get { return _Rect; }
        }

        public override void Init(Random rand)
        {
            Name = "pb";
            Y = rand.Next(1, 430);
            _Rect = new Rectangle(X, Y, _Widht, _Height);
        }

        public override void Update()
        {
            _Rect.X -= Speed;
        }
    }
}