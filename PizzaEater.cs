﻿using System.Collections.Generic;
using System.Drawing;

namespace BigFatPizzaGuy
{
    internal class PizzaEater
    {
        // Pizza Eater is Player object represenattion
        public int X { get; set; }

        public int Y { get; set; }
        public int Xsize { get; set; }
        public int Ysize { get; set; }
        public int Xspeed { get; set; }
        public int Yspeed { get; set; }
        public int Size { get; set; }
        private Rectangle _Rect;
        private Dictionary<string, PowerUP> Powers = new Dictionary<string, PowerUP>();

        public Rectangle Rect
        {
            get { return this._Rect; }
        }

        public void Initialize()
        {
            Xspeed = 0;
            Yspeed = 0;
            X = 50;
            Y = 230;
            Xsize = 20;
            Ysize = 20;
            Size = Xsize * Ysize;
            _Rect = new Rectangle(X, Y, Xsize, Ysize);
        }

        // update player position on screen
        public void Update()
        {
            X += Xspeed;
            Y += Yspeed;
            _Rect.X = X;
            _Rect.Y = Y;
            Move(Input.Direction());
        }

        public int Power(string power)
        {
            if (HasPower(power))
            {
                switch (power)
                {
                    case "pb": return 100;
                    case "boots": return 1;
                    case "stomach": return 5;
                }
            }

            return 0;
        }

        // check if player has powerup with given name or not
        private bool HasPower(string name)
        {
            PowerUP temp;
            if (Powers.TryGetValue(name, out temp))
                return true;
            return false;
        }

        // Eat pizza method - if Pizza Slice was eaten You need to grow xDD
        private int Grow = 1;

        public void Eat()
        {
            Xsize += Grow;
            Ysize += Grow;
            SizeRecalculation();
        }

        // Happens when You interesct with diet coke object
        public void DrinkDietCoke()
        {
            Xsize -= 2 * Grow;
            Ysize -= 2 * Grow;
            SizeRecalculation();
        }

        // happens when You intersect with Laxative object
        // total obliteration....
        public void EatLaxative()
        {
            Xsize = (int)(Xsize * 0.5);
            Ysize = Xsize;
            SizeRecalculation();
        }

        // this pizza was angry xD
        public void EatAngryPizza()
        {
            Xsize = 5;
            Ysize = 5;
            SPEED = 8;
            SizeRecalculation();
        }

        public void EatMedicine()
        {
            Xsize = 30 + Power("stomach");
            Ysize = 30 + Power("stomach");
            SPEED = 4;
            SizeRecalculation();
        }

        public void GainPower(PowerUP power)
        {
            if (!HasPower(power.Name))
                Powers.Add(power.Name, power);
        }

        public int UseDigestive(List<Pizza> pizzas)
        {
            int tempScore = 0;
            if (HasPower("digestive"))
            {
                foreach (var item in pizzas)
                {
                    if (item.Type == 0)
                        tempScore += (int)(item.Size * item.Speed * 0.01) + Power("pb");
                    Xsize += 1;
                    Ysize += 1;
                }
                SizeRecalculation();
                Powers.Remove("digestive");
                pizzas.Clear();
                return tempScore;
            }
            return 0;
        }

        private void SizeRecalculation()
        {
            Size = Xsize * Ysize;
            _Rect.Width = Xsize;
            _Rect.Height = Ysize;
        }

        // implementation of player movement

        private bool MovingUP = false;
        private bool MovingDOWN = false;
        private bool MovingLEFT = false;
        private bool MovingRIGHT = false;
        private bool MovingUPRIGHT = false;
        private bool MovingUPLEFT = false;
        private bool MovingDOWNRIGHT = false;
        private bool MovingDOWNLEFT = false;

        // check what direction was send to function then move in that direction
        // if no valid direction was set / no momvement key was down stop moving
        public void Move(Directions direction)
        {
            switch (direction)
            {
                case Directions.Up:
                    MoveUP();
                    break;

                case Directions.Down:
                    MoveDown();
                    break;

                case Directions.Left:
                    MoveLeft();
                    break;

                case Directions.Right:
                    MoveRight();
                    break;

                case Directions.UpLeft:
                    MoveUpLeft();
                    break;

                case Directions.UpRight:
                    MoveUpRight();
                    break;

                case Directions.DownLeft:
                    MoveDownLeft();
                    break;

                case Directions.DownRight:
                    MoveDownRight();
                    break;

                default:
                    Stop();
                    break;
            }
        }

        // basic speed
        private int SPEED = 4;

        private Dictionary<string, int> ScreenSize = new Dictionary<string, int>
        {
            { "TopEdge", 0 },
            { "BottomEdge", 479 },
            { "LeftEdge", 0 },
            { "RightEdge", 639 }
        };

        // set speed for movement
        private void MoveUP()
        {
            if (!MovingUP)
                Stop();
            // prevent moving off the screen
            if (Y > ScreenSize["TopEdge"])
            {
                Yspeed = -SPEED - Power("boots");
                MovingUP = true;
            }
            else
            {
                Y = ScreenSize["TopEdge"];
                Stop();
            }
        }

        private void MoveDown()
        {
            if (!MovingDOWN)
                Stop();
            if (Y + Ysize < ScreenSize["BottomEdge"])
            {
                Yspeed = SPEED + Power("boots");
                MovingDOWN = true;
            }
            else
            {
                Y = ScreenSize["BottomEdge"] - Ysize;
                Stop();
            }
        }

        private void MoveLeft()
        {
            if (!MovingLEFT)
                Stop();
            if (X > ScreenSize["LeftEdge"])
            {
                Xspeed = -SPEED - Power("boots");
                MovingLEFT = true;
            }
            else
            {
                X = ScreenSize["LeftEdge"];
                Stop();
            }
        }

        private void MoveRight()
        {
            if (!MovingRIGHT)
                Stop();
            if (X + Xsize < ScreenSize["RightEdge"])
            {
                Xspeed = SPEED + Power("boots");
                MovingRIGHT = true;
            }
            else
            {
                X = ScreenSize["RightEdge"] - Xsize;
                Stop();
            }
        }

        private void MoveUpLeft()
        {
            if (!MovingUPLEFT)
                Stop();
            if (X > ScreenSize["LeftEdge"] && Y > ScreenSize["TopEdge"])
            {
                Xspeed = -SPEED + 1;
                Yspeed = -SPEED + 1;
                MovingUPLEFT = true;
            }
            else
                Stop();
        }

        private void MoveUpRight()
        {
            if (!MovingUPRIGHT)
                Stop();
            if (X + Xsize < ScreenSize["RightEdge"] && Y > ScreenSize["TopEdge"])
            {
                Xspeed = SPEED - 1;
                Yspeed = -SPEED + 1;
                MovingUPRIGHT = true;
            }
            else
                Stop();
        }

        private void MoveDownLeft()
        {
            if (!MovingDOWNLEFT)
                Stop();
            if (X > ScreenSize["LeftEdge"] && Y + Ysize < ScreenSize["BottomEdge"])
            {
                Xspeed = -SPEED + 1;
                Yspeed = SPEED - 1;
                MovingDOWNLEFT = true;
            }
            else
                Stop();
        }

        private void MoveDownRight()
        {
            if (!MovingDOWNRIGHT)
                Stop();
            if (X + Xsize < ScreenSize["RightEdge"] && Y + Ysize < ScreenSize["BottomEdge"])
            {
                Xspeed = SPEED - 1;
                Yspeed = SPEED - 1;
                MovingDOWNRIGHT = true;
            }
            else
                Stop();
        }

        // set speed to 0 / stop moving
        private void Stop()
        {
            Xspeed = 0;
            Yspeed = 0;
            MovingUP = false;
            MovingDOWN = false;
            MovingLEFT = false;
            MovingRIGHT = false;
            MovingUPRIGHT = false;
            MovingUPLEFT = false;
            MovingDOWNRIGHT = false;
            MovingDOWNLEFT = false;
        }
    }
}