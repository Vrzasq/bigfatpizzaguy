﻿using System;
using System.Drawing;

namespace BigFatPizzaGuy
{
    internal class Medicine : Pizza
    {
        private Random rand;

        public Medicine(Random rand) : base(rand)
        {
            Xsize = 20;
            Ysize = 20;
            X = 500;
            Type = 4;
            _Rect = new Rectangle(X, Y, Xsize, Ysize);
            Speed = 5;
            this.rand = rand;
        }

        public override void Update()
        {
            Move();
            _Rect.X = X;
            _Rect.Y = Y;
        }

        private Directions direction = Directions.Left;
        private int frameCounter = 0;

        private void Move()
        {
            if (frameCounter > 60)
            {
                direction = (Directions)rand.Next(0, 8);
                frameCounter = 0;
            }
            frameCounter++;

            switch (direction)
            {
                case Directions.Up:
                    MoveUp();
                    break;

                case Directions.Down:
                    MoveDown();
                    break;

                case Directions.Left:
                    MoveLeft();
                    break;

                case Directions.Right:
                    MoveRight();
                    break;

                case Directions.UpLeft:
                    MoveUpLeft();
                    break;

                case Directions.UpRight:
                    MoveUpRight();
                    break;

                case Directions.DownLeft:
                    MoveDownLeft();
                    break;

                case Directions.DownRight:
                    MoveDownRight();
                    break;
            }
        }

        private void MoveUp()
        {
            if (Y > 0)
                Y -= Speed;
            else
            {
                frameCounter = 61;
                Move();
            }
        }

        private void MoveDown()
        {
            if (Y + Ysize < 479)
                Y += Speed;
            else
            {
                frameCounter = 61;
                Move();
            }
        }

        private void MoveLeft()
        {
            if (X > 0)
                X -= Speed;
            else
            {
                frameCounter = 61;
                Move();
            }
        }

        private void MoveRight()
        {
            if (X + Xsize < 639)
                X += Speed;
            else
            {
                frameCounter = 61;
                Move();
            }
        }

        private void MoveUpLeft()
        {
            if (X > 0 && Y > 0)
            {
                X -= Speed;
                Y -= Speed;
            }
            else
            {
                frameCounter = 61;
                Move();
            }
        }

        private void MoveUpRight()
        {
            if (X + Xsize < 639 && Y > 0)
            {
                X += Speed;
                Y -= Speed;
            }
            else
            {
                frameCounter = 61;
                Move();
            }
        }

        private void MoveDownLeft()
        {
            if (X > 0 && Y + Ysize < 479)
            {
                X -= Speed;
                Y += Speed;
            }
            else
            {
                frameCounter++;
                Move();
            }
        }

        private void MoveDownRight()
        {
            if (X + Xsize < 639 && Y + Ysize < 479)
            {
                X += Speed;
                Y += Speed;
            }
            else
            {
                frameCounter++;
                Move();
            }
        }
    }
}