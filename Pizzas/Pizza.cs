﻿using System;
using System.Drawing;

namespace BigFatPizzaGuy
{
    public class Pizza
    {
        public int Speed { get; set; }
        public int Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Xsize { get; set; }
        public int Ysize { get; set; }
        public int Size { get; set; }
        protected Rectangle _Rect;

        public Rectangle Rect
        {
            get { return _Rect; }
        }

        public Pizza(Random rand)
        {
            X = 639; // each pizza pice start moving Left from rigt edge of the screen
            Y = rand.Next(1, 420); // on random height
            Speed = rand.Next(1, 6); // with random speed xD
            Xsize = rand.Next(5, 81);
            Ysize = Xsize;
            _Rect = new Rectangle(X, Y, Xsize, Ysize);
            Size = Xsize * Ysize;
            Type = 0;
        }

        public virtual void Update()
        {
            X -= Speed;
            _Rect.X = X;
        }

        // place holder for frames counter in angrypizza class
        public virtual int AnimationFrame() { return 0; }
    }
}