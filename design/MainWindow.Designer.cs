﻿namespace BigFatPizzaGuy
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.pb_GameWindow = new System.Windows.Forms.PictureBox();
            this.lbl_ScoreText = new System.Windows.Forms.Label();
            this.lbl_Score = new System.Windows.Forms.Label();
            this.btn_Quit = new System.Windows.Forms.Button();
            this.btn_Restart = new System.Windows.Forms.Button();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.btn_Start = new System.Windows.Forms.Button();
            this.lbl_Weclome = new System.Windows.Forms.Label();
            this.lbl_GameOver = new System.Windows.Forms.Label();
            this.btn_Pause = new System.Windows.Forms.Button();
            this.lbl_AngryPizza = new System.Windows.Forms.Label();
            this.btn_HowToPlay = new System.Windows.Forms.Button();
            this.btn_Ladder = new System.Windows.Forms.Button();
            this.pb_Boots = new System.Windows.Forms.PictureBox();
            this.pb_Stomach = new System.Windows.Forms.PictureBox();
            this.pb_Digestive = new System.Windows.Forms.PictureBox();
            this.pb_Points = new System.Windows.Forms.PictureBox();
            this.lbl_Points = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.lbl_Boots = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.lbl_Stomach = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.lbl_Digestive = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_GameWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Boots)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Stomach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Digestive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Points)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_GameWindow
            // 
            this.pb_GameWindow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_GameWindow.Location = new System.Drawing.Point(27, 42);
            this.pb_GameWindow.Name = "pb_GameWindow";
            this.pb_GameWindow.Size = new System.Drawing.Size(640, 480);
            this.pb_GameWindow.TabIndex = 0;
            this.pb_GameWindow.TabStop = false;
            this.pb_GameWindow.WaitOnLoad = true;
            // 
            // lbl_ScoreText
            // 
            this.lbl_ScoreText.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ScoreText.Location = new System.Drawing.Point(673, 42);
            this.lbl_ScoreText.Name = "lbl_ScoreText";
            this.lbl_ScoreText.Size = new System.Drawing.Size(88, 32);
            this.lbl_ScoreText.TabIndex = 1;
            this.lbl_ScoreText.Text = "SCORE:";
            // 
            // lbl_Score
            // 
            this.lbl_Score.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Score.Location = new System.Drawing.Point(759, 42);
            this.lbl_Score.Name = "lbl_Score";
            this.lbl_Score.Size = new System.Drawing.Size(88, 32);
            this.lbl_Score.TabIndex = 1;
            // 
            // btn_Quit
            // 
            this.btn_Quit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Quit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Quit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Quit.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_Quit.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Quit.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Quit.Location = new System.Drawing.Point(683, 477);
            this.btn_Quit.Name = "btn_Quit";
            this.btn_Quit.Size = new System.Drawing.Size(164, 45);
            this.btn_Quit.TabIndex = 2;
            this.btn_Quit.Text = "Quit";
            this.btn_Quit.UseVisualStyleBackColor = true;
            this.btn_Quit.Click += new System.EventHandler(this.btn_Quit_Click);
            // 
            // btn_Restart
            // 
            this.btn_Restart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Restart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Restart.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Restart.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_Restart.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Restart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Restart.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Restart.Location = new System.Drawing.Point(683, 426);
            this.btn_Restart.Name = "btn_Restart";
            this.btn_Restart.Size = new System.Drawing.Size(164, 45);
            this.btn_Restart.TabIndex = 2;
            this.btn_Restart.Text = "Restart";
            this.btn_Restart.UseVisualStyleBackColor = true;
            this.btn_Restart.Visible = false;
            this.btn_Restart.Click += new System.EventHandler(this.btn_Restart_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Start.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Start.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_Start.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Start.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Start.Location = new System.Drawing.Point(683, 375);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(164, 45);
            this.btn_Start.TabIndex = 2;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // lbl_Weclome
            // 
            this.lbl_Weclome.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_Weclome.Location = new System.Drawing.Point(157, 234);
            this.lbl_Weclome.Name = "lbl_Weclome";
            this.lbl_Weclome.Size = new System.Drawing.Size(366, 66);
            this.lbl_Weclome.TabIndex = 3;
            this.lbl_Weclome.Text = "Press START to begin";
            this.lbl_Weclome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_GameOver
            // 
            this.lbl_GameOver.Font = new System.Drawing.Font("Showcard Gothic", 15.75F);
            this.lbl_GameOver.ForeColor = System.Drawing.Color.Firebrick;
            this.lbl_GameOver.Location = new System.Drawing.Point(157, 318);
            this.lbl_GameOver.Name = "lbl_GameOver";
            this.lbl_GameOver.Size = new System.Drawing.Size(366, 66);
            this.lbl_GameOver.TabIndex = 3;
            this.lbl_GameOver.Text = "Game Over\r\nPress Restart";
            this.lbl_GameOver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_GameOver.Visible = false;
            // 
            // btn_Pause
            // 
            this.btn_Pause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Pause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Pause.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_Pause.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Pause.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Pause.Location = new System.Drawing.Point(683, 324);
            this.btn_Pause.Name = "btn_Pause";
            this.btn_Pause.Size = new System.Drawing.Size(164, 45);
            this.btn_Pause.TabIndex = 2;
            this.btn_Pause.Text = "Pause";
            this.btn_Pause.UseVisualStyleBackColor = true;
            this.btn_Pause.Visible = false;
            this.btn_Pause.Click += new System.EventHandler(this.btn_Pause_Click);
            // 
            // lbl_AngryPizza
            // 
            this.lbl_AngryPizza.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_AngryPizza.ForeColor = System.Drawing.Color.Firebrick;
            this.lbl_AngryPizza.Location = new System.Drawing.Point(27, 7);
            this.lbl_AngryPizza.Name = "lbl_AngryPizza";
            this.lbl_AngryPizza.Size = new System.Drawing.Size(640, 32);
            this.lbl_AngryPizza.TabIndex = 1;
            this.lbl_AngryPizza.Text = "You have eaten angry pizza, Try to Catch medicine";
            this.lbl_AngryPizza.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_AngryPizza.Visible = false;
            // 
            // btn_HowToPlay
            // 
            this.btn_HowToPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_HowToPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_HowToPlay.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_HowToPlay.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_HowToPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_HowToPlay.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_HowToPlay.Location = new System.Drawing.Point(683, 149);
            this.btn_HowToPlay.Name = "btn_HowToPlay";
            this.btn_HowToPlay.Size = new System.Drawing.Size(164, 73);
            this.btn_HowToPlay.TabIndex = 2;
            this.btn_HowToPlay.Text = "How To Play";
            this.btn_HowToPlay.UseVisualStyleBackColor = true;
            this.btn_HowToPlay.Click += new System.EventHandler(this.btn_HowToPlay_Click);
            // 
            // btn_Ladder
            // 
            this.btn_Ladder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Ladder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Ladder.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btn_Ladder.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btn_Ladder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Ladder.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Ladder.Location = new System.Drawing.Point(678, 228);
            this.btn_Ladder.Name = "btn_Ladder";
            this.btn_Ladder.Size = new System.Drawing.Size(183, 45);
            this.btn_Ladder.TabIndex = 2;
            this.btn_Ladder.Text = "Ladderboard";
            this.btn_Ladder.UseVisualStyleBackColor = true;
            this.btn_Ladder.Click += new System.EventHandler(this.button1_Click);
            // 
            // pb_Boots
            // 
            this.pb_Boots.ImageLocation = "assets\\\\pu\\\\boots.jpg";
            this.pb_Boots.Location = new System.Drawing.Point(194, 560);
            this.pb_Boots.Name = "pb_Boots";
            this.pb_Boots.Size = new System.Drawing.Size(75, 75);
            this.pb_Boots.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Boots.TabIndex = 4;
            this.pb_Boots.TabStop = false;
            this.pb_Boots.Visible = false;
            // 
            // pb_Stomach
            // 
            this.pb_Stomach.ImageLocation = "assets\\\\pu\\\\stomach.jpg";
            this.pb_Stomach.Location = new System.Drawing.Point(37, 560);
            this.pb_Stomach.Name = "pb_Stomach";
            this.pb_Stomach.Size = new System.Drawing.Size(75, 75);
            this.pb_Stomach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Stomach.TabIndex = 4;
            this.pb_Stomach.TabStop = false;
            this.pb_Stomach.Visible = false;
            // 
            // pb_Digestive
            // 
            this.pb_Digestive.ImageLocation = "assets\\\\pu\\\\digestive.jpg";
            this.pb_Digestive.Location = new System.Drawing.Point(581, 560);
            this.pb_Digestive.Name = "pb_Digestive";
            this.pb_Digestive.Size = new System.Drawing.Size(75, 75);
            this.pb_Digestive.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Digestive.TabIndex = 4;
            this.pb_Digestive.TabStop = false;
            this.pb_Digestive.Visible = false;
            // 
            // pb_Points
            // 
            this.pb_Points.ImageLocation = "assets\\\\pu\\\\points.jpg";
            this.pb_Points.Location = new System.Drawing.Point(345, 560);
            this.pb_Points.Name = "pb_Points";
            this.pb_Points.Size = new System.Drawing.Size(150, 75);
            this.pb_Points.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_Points.TabIndex = 4;
            this.pb_Points.TabStop = false;
            this.pb_Points.Visible = false;
            // 
            // lbl_Points
            // 
            this.lbl_Points.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Points.Location = new System.Drawing.Point(340, 525);
            this.lbl_Points.Name = "lbl_Points";
            this.lbl_Points.Size = new System.Drawing.Size(174, 32);
            this.lbl_Points.TabIndex = 1;
            this.lbl_Points.Text = "Points +100";
            this.lbl_Points.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Points.Visible = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(27, 1056);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(174, 100);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // lbl_Boots
            // 
            this.lbl_Boots.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Boots.Location = new System.Drawing.Point(180, 525);
            this.lbl_Boots.Name = "lbl_Boots";
            this.lbl_Boots.Size = new System.Drawing.Size(100, 32);
            this.lbl_Boots.TabIndex = 1;
            this.lbl_Boots.Text = "Speed +1";
            this.lbl_Boots.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Boots.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new System.Drawing.Point(-156, 1056);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(174, 100);
            this.pictureBox6.TabIndex = 4;
            this.pictureBox6.TabStop = false;
            // 
            // lbl_Stomach
            // 
            this.lbl_Stomach.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Stomach.Location = new System.Drawing.Point(32, 525);
            this.lbl_Stomach.Name = "lbl_Stomach";
            this.lbl_Stomach.Size = new System.Drawing.Size(100, 32);
            this.lbl_Stomach.TabIndex = 1;
            this.lbl_Stomach.Text = "Size +5";
            this.lbl_Stomach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Stomach.Visible = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new System.Drawing.Point(-304, 1056);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(174, 100);
            this.pictureBox7.TabIndex = 4;
            this.pictureBox7.TabStop = false;
            // 
            // lbl_Digestive
            // 
            this.lbl_Digestive.Font = new System.Drawing.Font("Showcard Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Digestive.Location = new System.Drawing.Point(567, 525);
            this.lbl_Digestive.Name = "lbl_Digestive";
            this.lbl_Digestive.Size = new System.Drawing.Size(100, 32);
            this.lbl_Digestive.TabIndex = 1;
            this.lbl_Digestive.Text = "Ulti";
            this.lbl_Digestive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Digestive.Visible = false;
            // 
            // MainWindow
            // 
            this.AcceptButton = this.btn_Start;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_Quit;
            this.ClientSize = new System.Drawing.Size(873, 646);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pb_Points);
            this.Controls.Add(this.pb_Digestive);
            this.Controls.Add(this.pb_Stomach);
            this.Controls.Add(this.pb_Boots);
            this.Controls.Add(this.lbl_GameOver);
            this.Controls.Add(this.lbl_Weclome);
            this.Controls.Add(this.btn_HowToPlay);
            this.Controls.Add(this.btn_Ladder);
            this.Controls.Add(this.btn_Pause);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.btn_Restart);
            this.Controls.Add(this.btn_Quit);
            this.Controls.Add(this.lbl_Score);
            this.Controls.Add(this.lbl_AngryPizza);
            this.Controls.Add(this.lbl_Stomach);
            this.Controls.Add(this.lbl_Boots);
            this.Controls.Add(this.lbl_Digestive);
            this.Controls.Add(this.lbl_Points);
            this.Controls.Add(this.lbl_ScoreText);
            this.Controls.Add(this.pb_GameWindow);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainWindow";
            this.Text = "Big Fat Pizza Guy";
            this.Deactivate += new System.EventHandler(this.MainWindow_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainWindow_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MainWindow_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pb_GameWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Boots)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Stomach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Digestive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Points)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_GameWindow;
        private System.Windows.Forms.Label lbl_ScoreText;
        private System.Windows.Forms.Label lbl_Score;
        private System.Windows.Forms.Button btn_Quit;
        private System.Windows.Forms.Button btn_Restart;
        private System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.Label lbl_Weclome;
        private System.Windows.Forms.Label lbl_GameOver;
        private System.Windows.Forms.Button btn_Pause;
        private System.Windows.Forms.Label lbl_AngryPizza;
        private System.Windows.Forms.Button btn_HowToPlay;
        private System.Windows.Forms.Button btn_Ladder;
        private System.Windows.Forms.PictureBox pb_Boots;
        private System.Windows.Forms.PictureBox pb_Stomach;
        private System.Windows.Forms.PictureBox pb_Digestive;
        private System.Windows.Forms.PictureBox pb_Points;
        private System.Windows.Forms.Label lbl_Points;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label lbl_Boots;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label lbl_Stomach;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label lbl_Digestive;
    }
}

