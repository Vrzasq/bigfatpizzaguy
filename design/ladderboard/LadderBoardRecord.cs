﻿using System.Collections;
using System.Collections.Generic;

namespace BigFatPizzaGuy
{
    // implementing interface to sort ladder board records
    internal class SortLadderBoardRecordDescending : IComparer<LadderBoardRecord>
    {
        int IComparer<LadderBoardRecord>.Compare(LadderBoardRecord a, LadderBoardRecord b)
        {
            var record1 = a;
            var record2 = b;
            if (record1.Score < record2.Score)
                return 1;
            if (record1.Score > record2.Score)
                return -1;
            else
                return 0;
        }

        public static IComparer SortLadderDescending()
        {
            return (IComparer)new SortLadderBoardRecordDescending();
        }
    }

    internal class LadderBoardRecord
    {
        public string Name { get; set; }
        public int Score { get; set; }
        public string Date { get; set; }
    }
}