﻿using System;
using System.Drawing;

namespace BigFatPizzaGuy
{
    partial class Game
    {
        // basicly Draw everything
        // remember Drawing Order / Layers....
        private void Draw()
        {
            Bitmap frame = new Bitmap(640, 480);
            Graphics g = Graphics.FromImage(frame);
            g.DrawImage(PizzaGuy[ChoseDirectionForImage(Input.Direction())], player.X, player.Y, player.Xsize, player.Ysize);
            //g.DrawRectangle(Pens.Black, player.Rect);

            foreach (Pizza item in Pizzas)
            {
                if (item.Type == 3)
                {
                    // rotate angry pizza
                    g.DrawImage(APimages[item.AnimationFrame()], item.X, item.Y, item.Xsize, item.Ysize);
                }
                else
                    g.DrawImage(Foods[item.Type], item.X, item.Y, item.Xsize, item.Ysize);

                g.DrawRectangle(Pens.Black, item.Rect); // rectangles was fucked up
            }

            foreach (PowerUP item in PowerUps)
            {
                g.DrawImage(PowerUpsImages[item.Name], item.Rect);
                g.DrawRectangle(Pens.Crimson, item.Rect);
            }
                
            // relese picture box memory
            if (MainWindow.GameWindow != null)
                MainWindow.GameWindow.Dispose();

            // assign new frame to picture box
            MainWindow.GameWindow = frame;

            // relese graphic object memory
            g.Dispose();
        }

        // frame counter to define how often new pizza shoud be generated
        // basicly 60 frames means 1s
        private int frameCounter = 0;

        private int FramesToNewPizza = 30;

        // Update game state
        private void Update()
        {
            // CheckForCollision();

            player.Update();

            foreach (Pizza item in Pizzas)
                item.Update();

            foreach (PowerUP item in PowerUps)
                item.Update();

            CheckForCollision();

            frameCounter++;
            if (frameCounter >= FramesToNewPizza)
            {
                GeneratePizza();
                frameCounter = 0;
            }

            MainWindow.SetScore = Score;
        }

        // game loop
        public void GameState(object Sender, EventArgs e)
        {
            if (!GameOver && !Paused)
            {
                // MainWindow.RefreshImage();
                Update();
                Draw();
            }
            if (Score != 0 && GameOver)
            {
                int tempScore = Score;
                Score = 0;
                LadderBoardFileHandler.AddNewRecord(tempScore);
            }
        }
    }
}