﻿using System;
using System.Drawing;

namespace BigFatPizzaGuy
{
    internal class AngryPizza : Pizza
    {
        // frame counter required for animations
        private int _FrameCounter;

        public AngryPizza(Random rand) : base(rand)
        {
            _FrameCounter = 0;
            Type = 3;
            Speed = 5;
            _Rect = new Rectangle(X, Y, Xsize, Ysize);
        }

        public override void Update()
        {
            base.Update();
            _FrameCounter++;
        }

        private int _FramesToSwitchAnimation = 5;

        public override int AnimationFrame()
        {
            if (_FrameCounter < _FramesToSwitchAnimation)
                return 1;
            else if (_FrameCounter < _FramesToSwitchAnimation * 2)
                return 2;
            else if (_FrameCounter < _FramesToSwitchAnimation * 3)
                return 3;
            else if (_FrameCounter < _FramesToSwitchAnimation * 4)
                return 4;
            else if (_FrameCounter < _FramesToSwitchAnimation * 5)
                return 5;
            else if (_FrameCounter < _FramesToSwitchAnimation * 6)
                return 6;
            else if (_FrameCounter < _FramesToSwitchAnimation * 7)
                return 7;
            else if (_FrameCounter < _FramesToSwitchAnimation * 8)
                return 8;
            else
            {
                _FrameCounter = 0;
                return AnimationFrame();
            }
        }
    }
}