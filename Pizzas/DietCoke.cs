﻿using System;
using System.Drawing;

namespace BigFatPizzaGuy
{
    internal class DietCoke : Pizza
    {
        // it will shirnk pizza guy
        public DietCoke(Random rand) : base(rand)
        {
            Xsize = 20;
            Ysize = 30;
            Speed = 3;
            Type = 2;
            _Rect = new Rectangle(X, Y, Xsize, Ysize);
        }
    }
}