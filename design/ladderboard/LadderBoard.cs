﻿using System.Windows.Forms;

namespace BigFatPizzaGuy
{
    public partial class LadderBoard : Form
    {
        public LadderBoard()
        {
            InitializeComponent();

            int i = 1;
            foreach (var item in LadderBoardFileHandler.Ladder)
            {
                this.Controls["lbl_Score" + i].Text = item.Score.ToString();
                this.Controls["lbl_Name" + i].Text = item.Name;
                this.Controls["lbl_Date" + i].Text = item.Date;
                i++;
            }
        }
    }
}