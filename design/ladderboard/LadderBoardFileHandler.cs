﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace BigFatPizzaGuy
{
    internal static class LadderBoardFileHandler
    {
        public static List<LadderBoardRecord> Ladder = new List<LadderBoardRecord>();
        private static SortLadderBoardRecordDescending sortLadder = new SortLadderBoardRecordDescending();

        public static void ReadLadderBoard()
        {
            Ladder.Clear();
            try
            {
                using (var fileStream = File.OpenRead("LadderBoardFile\\ladder.txt"))
                {
                    using (StreamReader sr = new StreamReader(fileStream))
                    {
                        string line = "";
                        while ((line = sr.ReadLine()) != null)
                        {
                            string[] records = new string[3];
                            records = line.Split(';');
                            Ladder.Add(new LadderBoardRecord { Score = int.Parse(records[0]), Name = records[1], Date = records[2] });
                            Ladder.Sort(sortLadder);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Could not open or find ladder.txt file");
                throw e;
            }
        }

        public static void AddNewRecord(int score)
        {
            List<LadderBoardRecord> tempLedder = new List<LadderBoardRecord>(Ladder);
            foreach (var item in tempLedder)
            {
                if (item.Score < score)
                {
                    string name = "";
                    while ((name = Interaction.InputBox("Type Name:", "Gratz xD", "player", 200, 200)).Length > 10)
                        MessageBox.Show("Max lenght of name is 10 letters");
                    Ladder.Add(new LadderBoardRecord { Score = score, Name = name, Date = DateTime.Today.ToShortDateString() });
                    Ladder.Sort(sortLadder);
                    if (Ladder.Count > 10)
                        Ladder.RemoveAt(10);

                    using (LadderBoard ladder = new LadderBoard())
                    {
                        ladder.Visible = true;
                        MessageBox.Show("Gratz xD", "Ladder Board", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    break;
                }
            }
        }

        public static void SaveLadderToFile()
        {
            // delete old file
            try
            {
                File.Delete("LadderBoardFile\\ladder.txt");
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to override ladder.txt");
                throw e;
            }

            //save Ladder List to file
            try
            {
                using (StreamWriter LadderFile = new StreamWriter("LadderBoardFile\\ladder.txt"))
                {
                    foreach (var item in Ladder)
                    {
                        LadderFile.WriteLine(item.Score.ToString() + ";" + item.Name + ";" + item.Date);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to save ladder.txt", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw e;
            }
        }
    }
}