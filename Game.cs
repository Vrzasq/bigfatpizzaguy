﻿using System;
using System.Collections.Generic;
using System.Drawing;
using BigFatPizzaGuy.powerups;

namespace BigFatPizzaGuy
{
    partial class Game
    {
        // Score - player Score
        public int Score { get; set; }

        public bool GameOver { get; set; }
        public bool Paused { get; set; }
        private readonly IYolo MainWindow;

        public int SetFPS(int fps)
        {
            return 1000 / fps;
        }

        public Game(IYolo form)
        {
            this.MainWindow = form;
            GameOver = true;
        }

        // define player
        private PizzaEater player;

        // Intizilize starting game state
        private Random rand;

        public void Initialize()
        {
            rand = new Random();
            Pizzas.Clear();
            PowerUps.Clear();
            Input.ClearInputTable();
            player = new PizzaEater();
            Score = 0;
            MainWindow.SetScore = Score;
            player.Initialize();
            MakeSomeStartingPizza();
        }

        // define list of pizza object
        private List<Pizza> Pizzas = new List<Pizza>();
        private List<PowerUP> PowerUps = new List<PowerUP>();

        // make some pizza when starting the game
        private void MakeSomeStartingPizza()
        {
            for (int i = 0; i < 12; i++)
                Pizzas.Add(new Pizza(rand));
        }

        // generate some more pizza or other stuff xDD
        private void GeneratePizza()
        {
            int WhatToMake = rand.Next(0, 1000);
            if (WhatToMake <= 50)
                Pizzas.Add(new Laxative(rand));
            else if (WhatToMake >= 51 && WhatToMake < 860)
            {
                for (int i = 0; i < 2; i++)
                    Pizzas.Add(new Pizza(rand));
            }
            else if (WhatToMake >= 860 && WhatToMake < 958)
                Pizzas.Add(new DietCoke(rand));
            else if (WhatToMake >= 958 && WhatToMake < 988)
                Pizzas.Add(new AngryPizza(rand));
            else
                GeneratePower();
        }


        // Will generate PowerUps and add to the list
        private void GeneratePower()
        {
            int WhatToMake = rand.Next(0, 4);

            switch (WhatToMake)
            {
                case 0: var boots = new Boots(); boots.Init(rand); PowerUps.Add(boots); break;
                case 1: var digestive = new Digestive(); digestive.Init(rand); PowerUps.Add(digestive); break;
                case 2: var stomach = new BiggerStomach(); stomach.Init(rand); PowerUps.Add(stomach); break;
                case 3: var pb = new PointBooster(); pb.Init(rand); PowerUps.Add(pb); break;
            }
        }

        // detect collision
        private void CheckForCollision()
        {
            // create new list for looping
            // if You try to delete item during loop You will get enumeration error
            List<Pizza> tempPizzas = new List<Pizza>(Pizzas);
            foreach (var item in tempPizzas)
            {
                // check for Pizza / wall Collision
                // if Pizza hit left wall it is removed from Pizzas Collection
                if (item.X <= -20 && item.Type != 4)
                {
                    Pizzas.Remove(item);
                }

                // check for pizza / player collision
                if (item.Rect.IntersectsWith(player.Rect))
                {
                    // check if pizza guy can eat pizza or not
                    // he can eat if he is bigger then pizza slice
                    // also implements behavior with other objecs xD
                    // Especialy AngryPizza and Laxative
                    if (player.Size > item.Size && item.Type == 0)
                    {
                        player.Eat();
                        Pizzas.Remove(item);
                        Score += (int)(item.Size * item.Speed * 0.01) + player.Power("pb");
                    }
                    else if (item.Type == 1)
                    {
                        player.EatLaxative();
                        Pizzas.Remove(item);
                    }
                    else if (item.Type == 2)
                    {
                        player.DrinkDietCoke();
                        Pizzas.Remove(item);
                        Score += 50;
                    }
                    else if (item.Type == 3)
                    {
                        player.EatAngryPizza();
                        MainWindow.ShowAngryPizzaMsg(true);
                        Pizzas.Remove(item);
                        for (int i = 0; i < 10; i++)
                            GeneratePizza();
                        Pizzas.Add(new Medicine(rand));
                        Score += 50;
                    }
                    else if (item.Type == 4)
                    {
                        Pizzas.Clear();
                        MainWindow.ShowAngryPizzaMsg(false);
                        player.EatMedicine();
                        Score += 250;
                    }
                    else
                    {
                        GameOver = true;
                        MainWindow.ShowGameOver(true);
                    }
                }
            }

            List<PowerUP> tempPowers = new List<PowerUP>(PowerUps);
            foreach (var item in tempPowers)
            {
                if (item.Rect.X <= -20)
                    PowerUps.Remove(item);

                if (item.Rect.IntersectsWith(player.Rect))
                {
                    player.GainPower(item);
                    ShowPower(item.Name, true);
                    PowerUps.Remove(item);
                }
            }
        }

        private void ShowPower(string power, bool state)
        {
            switch (power)
            {
                case "pb": MainWindow.ShowPB(state); break;
                case "boots": MainWindow.ShowBoots(state); break;
                case "digestive": MainWindow.ShowDigestive(state); break;
                case "stomach": MainWindow.ShowStomach(state); break;
                default: break;
            }
        }

        public void PlayerUseDigestive()
        {
            Score += player.UseDigestive(Pizzas);
        }

        private Bitmap PizzaGuy_up;
        private Bitmap PizzaGuy_down;
        private Bitmap PizzaGuy_left;
        private Bitmap PizzaGuy_right;
        private Bitmap PizzaSlice;
        private Bitmap Laxative;
        private Bitmap DietCoke;
        private Bitmap Medicine;
        private Bitmap Digestive, Stomach, Boots, Points;
        private Bitmap ap1, ap2, ap3, ap4, ap5, ap6, ap7, ap8;
        private Dictionary<int, Bitmap> Foods = new Dictionary<int, Bitmap>();
        private Dictionary<Directions, Bitmap> PizzaGuy = new Dictionary<Directions, Bitmap>();
        private Dictionary<int, Bitmap> APimages;
        private Dictionary<string, Bitmap> PowerUpsImages;

        // load all graphics and sounds
        public void LoadAsstes()
        {
            try
            {
                SetImage(out PizzaGuy_up, "assets\\pizza_guy_up.jpg");
                SetImage(out PizzaGuy_down, "assets\\pizza_guy_down.jpg");
                SetImage(out PizzaGuy_left, "assets\\pizza_guy_left.jpg");
                SetImage(out PizzaGuy_right, "assets\\pizza_guy_right.jpg");
                SetImage(out PizzaSlice, "assets\\pizza_slice.jpg");
                SetImage(out Laxative, "assets\\laxative.jpg");
                SetImage(out DietCoke, "assets\\diet_coke.jpg");
                SetImage(out Medicine, "assets\\medicine.jpg");
                SetImage(out ap1, "assets\\ap\\ap1.jpg");
                SetImage(out ap2, "assets\\ap\\ap2.jpg");
                SetImage(out ap3, "assets\\ap\\ap3.jpg");
                SetImage(out ap4, "assets\\ap\\ap4.jpg");
                SetImage(out ap5, "assets\\ap\\ap5.jpg");
                SetImage(out ap6, "assets\\ap\\ap6.jpg");
                SetImage(out ap7, "assets\\ap\\ap7.jpg");
                SetImage(out ap8, "assets\\ap\\ap8.jpg");
                SetImage(out Digestive, "assets\\pu\\digestive.jpg");
                SetImage(out Boots, "assets\\pu\\boots.jpg");
                SetImage(out Points, "assets\\pu\\points.jpg");
                SetImage(out Stomach, "assets\\pu\\stomach.jpg");

                Foods.Add(0, PizzaSlice);
                Foods.Add(1, Laxative);
                Foods.Add(2, DietCoke);
                Foods.Add(3, PizzaSlice);
                Foods.Add(4, Medicine);

                PizzaGuy.Add(Directions.Up, PizzaGuy_up);
                PizzaGuy.Add(Directions.Down, PizzaGuy_down);
                PizzaGuy.Add(Directions.Left, PizzaGuy_left);
                PizzaGuy.Add(Directions.Right, PizzaGuy_right);

                APimages = new Dictionary<int, Bitmap>
                { { 1, ap1 }, { 2, ap2 }, { 3, ap3 }, { 4, ap4 }, { 5, ap5 }, { 6, ap6 }, { 7, ap7 }, { 8, ap8 } };

                PowerUpsImages = new Dictionary<string, Bitmap>
                { { "digestive", Digestive }, { "boots", Boots }, { "pb", Points}, { "stomach", Stomach } };
            }
            catch (Exception e)
            {
                Console.WriteLine("Fail to load asstes.");
                throw e;
            }
        }

        private void SetImage(out Bitmap bmp, string filePath)
        {
            Image tempImage = Image.FromFile(filePath);
            bmp = new Bitmap(tempImage);
            tempImage.Dispose();
            tempImage = null;
        }

        // return direction for image
        // the idea behind this is to skip all non basic direcitions
        private Directions ChoseDirectionForImage(Directions direction)
        {
            switch (direction)
            {
                case Directions.Down:
                    return Directions.Down;

                case Directions.Up:
                    return Directions.Up;

                case Directions.Left:
                    return Directions.Left;

                case Directions.Right:
                    return Directions.Right;

                default:
                    return Directions.Right;
            }
        }
    }
}