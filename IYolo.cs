﻿using System.Drawing;

namespace BigFatPizzaGuy
{
    internal interface IYolo
    {
        // help with connecting Main Window form with game Class
        int SetScore { set; }

        Image GameWindow { get; set; }

        void RefreshImage();

        void ShowGameOver(bool state);

        void ShowAngryPizzaMsg(bool state);
        void ShowDigestive(bool state);
        void ShowStomach(bool state);
        void ShowBoots(bool state);
        void ShowPB(bool state);
    }
}